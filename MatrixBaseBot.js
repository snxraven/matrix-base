const sdk = require("matrix-bot-sdk");
const MatrixClient = sdk.MatrixClient;
const SimpleFsStorageProvider = sdk.SimpleFsStorageProvider;
const AutojoinRoomsMixin = sdk.AutojoinRoomsMixin;

const homeserverUrl = "https://matrix-linux.cloud"; // make sure to update this with your url
const accessToken = "";
const storage = new SimpleFsStorageProvider("bot.json");
const client = new MatrixClient(homeserverUrl, accessToken, storage);
AutojoinRoomsMixin.setupOnClient(client);

function sendMessage(channel, message) {

    const content = {
        "body": message,
        "msgtype": "m.text",

    };
    client.sendEvent(channel, "m.room.message", content, "", (err, res) => {
        console.log(err).code;
    });
}


client.on("room.message", (roomId, event) => {
    activeChannel = roomId

    commandToRun = event.content.body.replace("^ ", "").replace("6", "")

    if (!event["content"]) return;
    const sender = event["sender"];
    const body = event["content"]["body"];

    if (event.unsigned.age > 300) return

    if (commandToRun == "ping") {
        sendMessage(activeChannel, "pong")
    }

})

client.start().then(() => console.log("Client started!"));
